package com.ef.service;

import com.ef.entity.AccessLog;

import java.util.List;

public interface AccessLogService {
    void addAccessLog(AccessLog accessLog);

    List getIpsInDateTimePeriode(String fromDate, String toDate, int threshold);

    List<AccessLog> findRequestsByIp(String ip);
}
