package com.ef.service;

import com.ef.entity.SavedResults;

public interface SavedResultsService {
    void addResult(SavedResults savedResults);
}
