package com.ef;

import com.ef.entity.AccessLog;
import com.ef.service.AccessLogServiceImp;
import com.ef.service.LogFileServiceImp;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Parser {

    public static void main(String[] args) {
        Map<Object, Object> mapArgs = Arrays.stream(args).map(s -> s.split("=")).collect(Collectors.toMap(e -> e[0].replace("--", ""), e -> e[1]));

        String startDate = (String) mapArgs.get("startDate");
        String duration = (String) mapArgs.get("duration");
        String accessLog = (String) mapArgs.get("accesslog");
        int threshold = Integer.parseInt((String) mapArgs.get("threshold"));
        String to = "";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss");
        LocalDateTime from = LocalDateTime.parse(startDate, formatter);

        if (duration.equals("hourly")) {
            if (threshold <= 200 && threshold > 0)
                to = from.plusHours(1).format(formatter);
            else {
                System.out.println("When duration is hourly , threshold must be between 0 and 200");
                System.exit(0);
            }
        } else if (duration.equals("daily")) {
            if (threshold <= 500 && threshold > 0)
                to = from.plusHours(24).format(formatter);
            else {
                System.out.println("When duration is daily , threshold must be between 0 and 500");
                System.exit(0);
            }
        } else {
            System.out.println("Duration value must be 'daily' or 'hourly'");
            System.exit(0);
        }

        LogFileServiceImp.getInstance().readLogFile(accessLog);
        List ipsList = AccessLogServiceImp.getInstance().getIpsInDateTimePeriode(startDate, to, threshold);
        List<AccessLog> listsByIp = AccessLogServiceImp.getInstance().findRequestsByIp("192.168.234.82");

        System.exit(0);
    }

}
