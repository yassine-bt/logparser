package com.ef.entity;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table
public class SavedResults {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer threshold;
    private String startDate;
    private String toDate;
    private BigInteger nbr;
    private String ip;
    private String comment;

    public SavedResults() {
    }

    public SavedResults(Integer threshold, String startDate, String toDate, BigInteger nbr, String ip, String comment) {
        this.threshold = threshold;
        this.startDate = startDate;
        this.toDate = toDate;
        this.nbr = nbr;
        this.ip = ip;
        this.comment = comment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String from) {
        this.startDate = from;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String to) {
        this.toDate = to;
    }

    public BigInteger getNbr() {
        return nbr;
    }

    public void setNbr(BigInteger nbr) {
        this.nbr = nbr;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "SavedResults{" +
                "id=" + id +
                ", threshold=" + threshold +
                ", from=" + startDate +
                ", to=" + toDate +
                ", nbr='" + nbr + '\'' +
                ", ip='" + ip + '\'' +
                ", comment='" + comment + '\'' +
                '}';
    }
}
