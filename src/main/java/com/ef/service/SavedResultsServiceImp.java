package com.ef.service;

import com.ef.config.HibernateUtils;
import com.ef.entity.SavedResults;
import org.hibernate.Session;

public class SavedResultsServiceImp implements SavedResultsService {

    private static SavedResultsServiceImp savedResultsService = new SavedResultsServiceImp();
    private Session session = HibernateUtils.getSessionFactory().openSession();

    static SavedResultsServiceImp getInstance() {
        return savedResultsService;
    }

    private SavedResultsServiceImp() {
    }

    @Override
    public void addResult(SavedResults savedResults) {
        session.save(savedResults);
    }

    void openTransaction() {
        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
    }

    void commit() {
        session.getTransaction().commit();
    }

    void close() {
        session.close();
    }
}
