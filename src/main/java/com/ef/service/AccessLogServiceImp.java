package com.ef.service;

import com.ef.config.HibernateUtils;
import com.ef.entity.AccessLog;
import com.ef.entity.SavedResults;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.math.BigInteger;
import java.util.List;

public class AccessLogServiceImp implements AccessLogService {

    private static AccessLogServiceImp accessLogService = new AccessLogServiceImp();
    private Session session = HibernateUtils.getSessionFactory().openSession();
    private SavedResultsServiceImp savedResultsService = SavedResultsServiceImp.getInstance();

    public static AccessLogServiceImp getInstance() {
        return accessLogService;
    }

    private AccessLogServiceImp() {
    }


    public void addAccessLog(AccessLog accessLog) {
        session.save(accessLog);
    }

    public List getIpsInDateTimePeriode(String fromDate, String toDate, int threshold) {
        openTransaction();
        Query query = session.createSQLQuery(
                "SELECT COUNT(ip) AS nbr, iP FROM AccessLog WHERE (date BETWEEN :fromDate AND :toDate ) GROUP BY iP HAVING COUNT(ip)> :threshold ORDER BY COUNT(ip) DESC")
                .setParameter("fromDate", fromDate)
                .setParameter("toDate", toDate)
                .setParameter("threshold", threshold);
        List<Object[]> result = query.list();
        close();

        System.out.println(result.size() + " results from " + fromDate + " to " + toDate + " with " + threshold + " as threshold param \n");

        savedResultsService.openTransaction();
        result.forEach(searchResult -> {
                    String comment = searchResult[0] + " found for IP " + searchResult[1] + " from " + fromDate + " to " + toDate + " with " + threshold + " as threshold";
                    SavedResults savedResult = new SavedResults(threshold, fromDate, toDate, (BigInteger) searchResult[0], (String) searchResult[1], comment);
                    System.out.println(searchResult[0] + ", " + searchResult[1]);
                    savedResultsService.addResult(savedResult);
                }
        );
        savedResultsService.commit();
        savedResultsService.close();

        return result;
    }

    public List<AccessLog> findRequestsByIp(String ip) {
        openTransaction();
        Query query = session.createSQLQuery(
                "SELECT * FROM AccessLog WHERE ip = :ip ")
                .addEntity(AccessLog.class)
                .setParameter("ip", ip);
        List<AccessLog> result = query.list();
        commit();
        close();

        System.out.println(result.size() + " results was found for " + ip);
        result.forEach(System.out::println);
        return result;

    }

    void openTransaction() {
        session = HibernateUtils.getSessionFactory().openSession();
        session.beginTransaction();
    }


    void commit() {
        session.getTransaction().commit();
    }

    void close() {
        session.close();
    }
}
