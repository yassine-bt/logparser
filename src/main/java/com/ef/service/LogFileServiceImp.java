package com.ef.service;

import com.ef.entity.AccessLog;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class LogFileServiceImp implements LogFileService {

    private static LogFileServiceImp logFileService = new LogFileServiceImp();
    private AccessLogServiceImp accessLogServiceImp = AccessLogServiceImp.getInstance();

    public static LogFileServiceImp getInstance() {
        return logFileService;
    }

    private LogFileServiceImp() {
    }

    /**
     * From here the logfile will be read
     *
     * @param file path to the logFile
     */
    public void readLogFile(String file) {

        try {
            Stream<String> lines = Files.lines(Paths.get(file));
            accessLogServiceImp.openTransaction();
            lines.forEach(line -> {
                AccessLog logLine = parseLine(line, "|");
                accessLogServiceImp.addAccessLog(logLine);
            });
            lines.close();
            accessLogServiceImp.commit();
            accessLogServiceImp.close();
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    /**
     * here the parse logic will be implemented
     *
     * @param line      line format is as follow : Date, IP, Request, Status, User Agent (pipe delimited, open the example file in text editor)
     * @param delimiter for this example we will use pipe |
     * @return AccessLog object to save in DB
     */
    private AccessLog parseLine(String line, String delimiter) {
        // Array will contains : Date, IP, Request, Status, UserAgent
        String[] parsed = line.split(Pattern.quote(delimiter));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        LocalDateTime formatDateTime = LocalDateTime.parse(parsed[0], formatter);
        return new AccessLog(formatDateTime, parsed[1], parsed[2], Integer.parseInt(parsed[3]), parsed[4]);
    }

}
